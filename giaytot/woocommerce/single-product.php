<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); ?>
<main id="main" class="container">
	<div class="row">
		<div id="primary" class="col-md-9 col-sm-8 pull-right">
			<?php
			dynamic_sidebar('ad-banner-top');
			while ( have_posts() ) : the_post();
				wc_get_template_part( 'content', 'single-product' );
			endwhile; ?>
		</div>
		<div id="secondary" class="col-md-3 col-sm-4">
			<?php get_sidebar(); ?>
		</div>
	</div>
</main>
<?php get_footer(); ?>
