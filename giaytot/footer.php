<?php if( !( is_cart() || is_checkout() || is_account_page() ) ): ?>
    <div class="after_main_desc">
        <div class="container">
            <?php
                $after_main = function_exists('fw_get_db_settings_option') ? fw_get_db_settings_option('after_main_desc', '') : '';
                echo wpautop( $after_main, true );
            ?>
        </div>
    </div>
<?php endif; ?>
<section class="ht-section ht-prefooter">
    <div class="container">
        <?php dynamic_sidebar('footer-widget-contact'); ?>
        <!-- /ht-meta-contact -->
        <?php dynamic_sidebar('footer-widget-product-tag'); ?>
        <!-- /ht-product-tag -->
        <?php dynamic_sidebar('footer-widget-links'); ?>
        <!-- /ht-quick-link -->
        <?php dynamic_sidebar('footer-widget-sub'); ?>
        <!-- /-ht-subscribe -->
    </div>
</section>
<footer id="site-footer">
    <div class="copyright">
        <div class="container">
            Copyright 2014. All Rights Reserved.
        </div>
    </div>
</footer>
<?php
$popup_content = function_exists('fw_get_db_settings_option') ? fw_get_db_settings_option('ad_popup', '') : '';
if( $popup_content != '' ):
    ?>
        <div class="ht-popup-ad">
            <a class="pop-up-button-open" href="#openAd" style="display: none;">Open it up!</a>
            <div id="openAd" class="modalbg">
                <div class="dialog">
                    <a href="#" title="Close" class="close">X</a>
                    <?php echo html_entity_decode($popup_content); ?>
                </div>
            </div>
        </div>
        <?php if( is_home() || is_shop() ): ?>
            <script>
                jQuery(document).ready(function($) {
                    setTimeout(function(){
                        window.location = $('.pop-up-button-open').attr('href');
                    }, <?php echo is_home() ? '16000' : '12000'; ?>);
                });
            </script>
        <?php endif; ?>
    <?php
endif;
?>
<?php wp_footer(); ?>

</body>
</html>
