<?php
get_header(); ?>
<main id="main" class="container">
	<div class="row">
		<?php if( !( is_woocommerce() || is_cart() || is_checkout() || is_account_page() ) ): ?>
			<div id="primary" class="col-md-9 col-sm-8 pull-right">
				<?php
					while ( have_posts() ) : the_post(); ?>
						<article <?php post_class(); ?>>
							<?php
								the_content();
							?>
						</article><!-- #post-## -->
					<?php
					endwhile; ?>
			</div>
			<div id="secondary" class="col-md-3 col-sm-4">
				<?php get_sidebar(); ?>
			</div>
		<?php else: ?>
			<div id="primary" class="col-sm-12">
				<?php
					while ( have_posts() ) : the_post(); ?>
						<article <?php post_class(); ?>>
							<?php
								the_content();
							?>
						</article><!-- #post-## -->
					<?php
					endwhile; ?>
			</div>
		<?php endif; ?>
	</div>
</main>
<?php
get_footer(); ?>
