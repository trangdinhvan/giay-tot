<?php
/**
 * The template for displaying 404 pages (Not Found)
 */

get_header(); ?>
<main id="main" class="container">
	<div class="row">
		<div class="col-sm-12">
			<h3>Lỗi 404: Không tìm thấy bài viết.</h3>
		</div>
	</div>
</main>
<?php
get_footer();
