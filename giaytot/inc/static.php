<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
/**
 * Include static files: javascript and css
 */

if ( is_admin() ) {
	return;
}

/**
 * Enqueue scripts and styles for the front end.
 */

// Add Lato font, used in the main stylesheet.
// wp_enqueue_style(
// 	'fw-theme-fonts-roboto',
// 	fw_theme_font_url(),
// 	array(),
// 	'1.0'
// );

wp_enqueue_style(
    'bootstrap',
    get_template_directory_uri() . '/css/bootstrap.min.css',
    array(),
    '1.0'
);

// Font Awesome stylesheet
wp_enqueue_style(
	'font-awesome',
	get_template_directory_uri() . '/css/font-awesome.min.css',
	array(),
	'1.0'
);

wp_enqueue_style(
    'css-plugins',
    get_template_directory_uri() . '/css/plugins.css',
    array(),
    '1.0'
);

/*Menu*/
wp_enqueue_style( 'response-menubar', get_template_directory_uri()."/css/menu-styles.css", false, 1.0 );
wp_enqueue_script( 'response-menubar-js', get_template_directory_uri()."/js/menu-script.js", 'jquery', 1.0, true );

// Load our main stylesheet.
wp_enqueue_style(
    'fw-theme-style',
    get_stylesheet_uri(),
    array('bootstrap','css-plugins'),
    '1.0'
);

wp_enqueue_script(
    'jquery-plugins',
    get_template_directory_uri() . '/js/plugins.js',
    array( 'jquery' ),
    '1.0',
    true
);

wp_enqueue_script(
	'custom-js',
	get_template_directory_uri() . '/js/custom.js',
	array( 'jquery' ),
	'1.0',
	true
);

