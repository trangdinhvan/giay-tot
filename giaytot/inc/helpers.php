<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
/**
 * Helper functions and classes with static methods for usage in theme
 */

/**
 * Register Lato Google font.
 *
 * @return string
 */
function fw_theme_font_url() {
	$fonts_url = '';

	/* Translators: If there are characters in your language that are not
    * supported by Open Sans, translate this to 'off'. Do not translate
    * into your own language.
    */
	$roboto = _x( 'on', 'Roboto font: on or off', 'mauris' );
	$condensed = _x( 'on', 'Roboto Condensed font: on or off', 'mauris' );

	if (  'off' !== $roboto || 'off' !== $condensed ) {
		$font_families = array();
		if ( 'off' !== $roboto ) {
			$font_families[] = 'Roboto:700,400';
		}
		if ( 'off' !== $condensed ) {
			$font_families[] = 'Roboto Condensed:700,400';
		}
		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,vietnamese' ),
		);
		$fonts_url = add_query_arg( $query_args, "//fonts.googleapis.com/css" );
	}

	return esc_url_raw( $fonts_url );
}

if ( ! function_exists('fw_theme_the_attached_image') ) : /**
 * Print the attached image with a link to the next attached image.
 */ {
	function fw_theme_the_attached_image() {
		$post = get_post();
		/**
		 * Filter the default attachment size.
		 *
		 * @param array $dimensions {
		 *     An array of height and width dimensions.
		 *
		 * @type int $height Height of the image in pixels. Default 810.
		 * @type int $width Width of the image in pixels. Default 810.
		 * }
		 */
		$attachment_size     = apply_filters( 'fw_theme_attachment_size', array( 810, 810 ) );
		$next_attachment_url = wp_get_attachment_url();

		/*
		 * Grab the IDs of all the image attachments in a gallery so we can get the URL
		 * of the next adjacent image in a gallery, or the first image (if we're
		 * looking at the last image in a gallery), or, in a gallery of one, just the
		 * link to that image file.
		 */
		$attachment_ids = get_posts( array(
			'post_parent'    => $post->post_parent,
			'fields'         => 'ids',
			'numberposts'    => - 1,
			'post_status'    => 'inherit',
			'post_type'      => 'attachment',
			'post_mime_type' => 'image',
			'order'          => 'ASC',
			'orderby'        => 'menu_order ID',
		) );

		// If there is more than 1 attachment in a gallery...
		if ( count( $attachment_ids ) > 1 ) {
			foreach ( $attachment_ids as $attachment_id ) {
				if ( $attachment_id == $post->ID ) {
					$next_id = current( $attachment_ids );
					break;
				}
			}

			// get the URL of the next image attachment...
			if ( $next_id ) {
				$next_attachment_url = get_attachment_link( $next_id );
			} // or get the URL of the first image attachment.
			else {
				$next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );
			}
		}

		printf( '<a href="%1$s" rel="attachment">%2$s</a>',
			esc_url( $next_attachment_url ),
			wp_get_attachment_image( $post->ID, $attachment_size )
		);
	}
}
endif;


/**
 * Custom template tags
 */
{
	if ( ! function_exists( 'fw_theme_paging_nav' ) ) : /**
	 * Display navigation to next/previous set of posts when applicable.
	 */ {
		function fw_theme_paging_nav( $wp_query = null ) {

			if ( ! $wp_query ) {
				$wp_query = $GLOBALS['wp_query'];
			}

			// Don't print empty markup if there's only one page.

			if ( $wp_query->max_num_pages < 2 ) {
				return;
			}

			$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
			$pagenum_link = html_entity_decode( get_pagenum_link() );
			$query_args   = array();
			$url_parts    = explode( '?', $pagenum_link );

			if ( isset( $url_parts[1] ) ) {
				wp_parse_str( $url_parts[1], $query_args );
			}

			$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
			$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

			$format = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link,
				'index.php' ) ? 'index.php/' : '';
			$format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%',
				'paged' ) : '?paged=%#%';

			// Set up paginated links.
			$links = paginate_links( array(
				'base'      => $pagenum_link,
				'format'    => $format,
				'total'     => $wp_query->max_num_pages,
				'current'   => $paged,
				'mid_size'  => 1,
				'add_args'  => array_map( 'urlencode', $query_args ),
				'prev_text' => __( '&larr; Previous', 'unyson' ),
				'next_text' => __( 'Next &rarr;', 'unyson' ),
			) );

			if ( $links ) :

				?>
				<nav class="navigation paging-navigation" role="navigation">
					<h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'unyson' ); ?></h1>

					<div class="pagination loop-pagination">
						<?php echo $links; ?>
					</div>
					<!-- .pagination -->
				</nav><!-- .navigation -->
			<?php
			endif;
		}
	}
	endif;

	if ( ! function_exists( 'fw_theme_post_nav' ) ) : /**
	 * Display navigation to next/previous post when applicable.
	 */ {
		function fw_theme_post_nav() {
			// Don't print empty markup if there's nowhere to navigate.
			$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '',
				true );
			$next     = get_adjacent_post( false, '', false );

			if ( ! $next && ! $previous ) {
				return;
			}

			?>
			<nav class="navigation post-navigation" role="navigation">
				<h1 class="screen-reader-text"><?php _e( 'Post navigation', 'unyson' ); ?></h1>

				<div class="nav-links">
					<?php
					if ( is_attachment() ) :
						previous_post_link( '%link',
							__( '<span class="meta-nav">Published In</span>%title', 'unyson' ) );
					else :
						previous_post_link( '%link',
							__( '<span class="meta-nav">Previous Post</span>%title', 'unyson' ) );
						next_post_link( '%link', __( '<span class="meta-nav">Next Post</span>%title', 'unyson' ) );
					endif;
					?>
				</div>
				<!-- .nav-links -->
			</nav><!-- .navigation -->
		<?php
		}
	}
	endif;

	if ( ! function_exists( 'fw_theme_posted_on' ) ) : /**
	 * Print HTML with meta information for the current post-date/time and author.
	 */ {
		function fw_theme_posted_on() {
			if ( is_sticky() && is_home() && ! is_paged() ) {
				echo '<span class="featured-post">' . __( 'Sticky', 'unyson' ) . '</span>';
			}

			// Set up and print post meta information.
			printf( '<span class="entry-date"><a href="%1$s" rel="bookmark"><time class="entry-date" datetime="%2$s">%3$s</time></a></span> <span class="byline"><span class="author vcard"><a class="url fn n" href="%4$s" rel="author">%5$s</a></span></span>',
				esc_url( get_permalink() ),
				esc_attr( get_the_date( 'c' ) ),
				esc_html( get_the_date() ),
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				get_the_author()
			);
		}
	}
	endif;

	/**
	 * Find out if blog has more than one category.
	 *
	 * @return boolean true if blog has more than 1 category
	 */
	function fw_theme_categorized_blog() {
		if ( false === ( $all_the_cool_cats = get_transient( 'fw_theme_category_count' ) ) ) {
			// Create an array of all the categories that are attached to posts
			$all_the_cool_cats = get_categories( array(
				'hide_empty' => 1,
			) );

			// Count the number of categories that are attached to the posts
			$all_the_cool_cats = count( $all_the_cool_cats );

			set_transient( 'fw_theme_category_count', $all_the_cool_cats );
		}

		if ( 1 !== (int) $all_the_cool_cats ) {
			// This blog has more than 1 category so fw_theme_categorized_blog should return true
			return true;
		} else {
			// This blog has only 1 category so fw_theme_categorized_blog should return false
			return false;
		}
	}

	/**
	 * Display an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index
	 * views, or a div element when on single views.
	 */
	function fw_theme_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		$current_position = false;
		if (function_exists('fw_ext_sidebars_get_current_position')) {
			$current_position = fw_ext_sidebars_get_current_position();
		}



		if ( is_singular() ) :
			?>

			<div class="post-thumbnail">
				<?php
				if ( ( in_array( $current_position,
						array( 'full', 'left' ) ) || is_page_template( 'page-templates/full-width.php' )
					|| empty($current_position)
				)
				) {
					the_post_thumbnail( 'fw-theme-full-width' );
				} else {
					the_post_thumbnail();
				}
				?>
			</div>

		<?php else : ?>

			<a class="post-thumbnail" href="<?php the_permalink(); ?>">
				<?php
				if ( ( in_array( $current_position,
						array( 'full', 'left' ) ) || is_page_template( 'page-templates/full-width.php' ) )
						|| empty($current_position)
				) {
					the_post_thumbnail( 'fw-theme-full-width' );
				} else {
					the_post_thumbnail();
				}
				?>
			</a>

		<?php endif; // End is_singular()
	}
}

/**
 * Custom template tags and functions by HAINTHEME
 */
{
	/**
	 * Custom comment output.
	 */
	function fw_mauris_comment($comment, $args, $depth)
	{
		$GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
		<div id="comment-<?php comment_ID(); ?>">

			<div class="comment-author vcard">
				<?php echo get_avatar(get_the_author_meta('ID'), $size = '72'); ?>
			</div>

			<div class="comment-content">

				<div class="comment-content-inner">
					<?php if ($comment->comment_approved == '0') : ?>
						<em><?php _e('Your comment is awaiting moderation.', 'mauris') ?></em>
						<br/>
					<?php endif; ?>

					<div class="metadata">
						<?php printf(__('<cite class="fn">%s</cite>', 'mauris'), get_comment_author_link()) ?>
						<p class="time"><a
								href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)) ?>">
								<?php printf(__('%1$s', 'mauris'), get_comment_date(), get_comment_time()) ?></a></p>
						<?php edit_comment_link(__('(Edit)', 'mauris'), '  ', '') ?>
						<?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
					</div>

					<div class="comment-text"><?php comment_text() ?></div>
				</div>
				<!-- /.comment-content-inner -->

			</div>
		</div>
	<?php
	}
}

function WooCountdownSale(){
	$thepostid = get_the_ID();
	$sale_price_dates_from  = ( $date = get_post_meta( $thepostid, '_sale_price_dates_from', true ) ) ? date( 'M d, Y 00:00:00', $date ) : '';
	$sale_price_dates_to  = ( $date = get_post_meta( $thepostid, '_sale_price_dates_to', true ) ) ? date( 'M d, Y 23:59:00', $date ) : '';
	$today = date('M d, Y 00:00:00');
	$counting = false;
	if( $sale_price_dates_to != '' ){//Has expired date
	    if( $sale_price_dates_from == '' ){ //Empty starting date, start from today
	        $counting = true;
	    }else{
	        if( !($sale_price_dates_from > $today) ){ //Sale started days before
	            $counting = true;
	        }
	    }
	}
	if( $counting ):
	ob_start();
	?>
		<div class="ht-countdown-hook" data-date="<?php echo $sale_price_dates_to; ?>" data-day-text="ngày" data-hour-text="giờ" data-min-text="phút" data-sec-text="giây"></div>
		<script>
			jQuery(document).ready(function($) {
				jQuery('.ht-countdown-hook').each(function() {
				    var $thisCountDown = jQuery(this);
				    var dayText = ($thisCountDown.data('day-text') == undefined) ? 'days' : $thisCountDown.data('day-text');
				    var hourText = ($thisCountDown.data('hour-text') == undefined) ? 'hours' : $thisCountDown.data('hour-text');
				    var minText = ($thisCountDown.data('min-text') == undefined) ? 'minutes' : $thisCountDown.data('min-text');
				    var secText = ($thisCountDown.data('sec-text') == undefined) ? 'secconds' : $thisCountDown.data('sec-text');
				    $thisCountDown.countdown({
				        date: jQuery(this).data('date'),
				        refresh: 1000,
				        <?php if( is_singular('product') ): ?>
					        render: function(data) {
					            jQuery(this.el).html(
					                "Còn &nbsp;<span class='ht_countdown_section'><span class='value'>" + this.leadingZeros(((data.years > 0) ? data.years * 365 : data.days) , 2) + " </span><span class='indicator'>" + dayText + " </span></span>"+
					                "<span class='ht_countdown_section'><span class='value'>" + this.leadingZeros(data.hours, 2) + " </span><span class='indicator'> " + hourText + "</span></span>"+
					                "<span class='ht_countdown_section'><span class='value'>" + this.leadingZeros(data.min, 2) + " </span><span class='indicator'>" + minText + "</span></span>"+
					                "<span class='ht_countdown_section'><span class='value'>" + this.leadingZeros(data.sec, 2) + " </span><span class='indicator'>" + secText + "</span></span><i class='fa fa-clock-o'></i>");
					        }
				        <?php else: ?>
				        	render: function(data) {
				        	    jQuery(this.el).html(
				        	        "<i class='fa fa-clock-o'></i> <span class='ht_countdown_section'><span class='value'>" + this.leadingZeros(((data.years > 0) ? data.years * 365 : data.days) , 2) + " </span><span class='indicator'>ngày </span></span>"+
				        	        "<span class='ht_countdown_section'><span class='value'>" + this.leadingZeros(data.hours, 2) + "</span><span class='indicator'>:</span></span>"+
				        	        "<span class='ht_countdown_section'><span class='value'>" + this.leadingZeros(data.min, 2) + "</span><span class='indicator'>:</span></span>"+
				        	        "<span class='ht_countdown_section'><span class='value'>" + this.leadingZeros(data.sec, 2) + "</span><span class='indicator'></span></span>");
				        	}
				        <?php endif; ?>
				    });
				});
			});
		</script>
	<?php
	$countdown_html = ob_get_clean();
	echo $countdown_html;
	endif;
}