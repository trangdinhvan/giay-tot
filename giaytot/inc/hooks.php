<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
/**
 * Filters and Actions
 */


if ( ! function_exists( '_action_theme_setup' ) ) : /**
 * Theme setup.
 *
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 * @internal
 */ {
	function _action_theme_setup() {

		/*
		 * Make Theme available for translation.
		 */
		load_theme_textdomain( 'unyson', get_template_directory() . '/languages' );
		// Add RSS feed links to <head> for posts and comments.
		add_theme_support( 'automatic-feed-links' );
		// Enable support for Post Thumbnails, and declare two sizes.
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 100, 100, true );
		//add_image_size( 'fw-theme-full-width', 1038, 576, true );
		add_theme_support('title-tag');
		//Set to 0 to remove default thumbnail size
		//use wp_get_attachment_image_src( $attachment_id, $size, $icon ); to retrieve the image size = thumbnail, medium, large
		update_option( 'thumbnail_size_h', 100 );
		update_option( 'thumbnail_size_w', 100 );
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			// 'gallery',
			// 'caption'
		) );
	}
}
endif;
add_action( 'after_setup_theme', '_action_theme_setup' );


/**
 * Register widget areas.
 * @internal
 */
function _action_theme_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Banner đỉnh', 'unyson' ),
		'id'            => 'ad-banner-top',
		'description'   => __( 'Banner quảng cáo ở đỉnh trang', 'unyson' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Khối Liên hệ', 'unyson' ),
		'id'            => 'footer-widget-contact',
		'description'   => __( 'Liên hệ', 'unyson' ),
		'before_widget' => '<div class="ht-widget-item ht-meta-contact">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Khối tag sản phẩm', 'unyson' ),
		'id'            => 'footer-widget-product-tag',
		'description'   => __( 'Mây thẻ sản phẩm', 'unyson' ),
		'before_widget' => '<div class="ht-widget-item ht-product-tag">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Khối Liên kết', 'unyson' ),
		'id'            => 'footer-widget-links',
		'description'   => __( 'Các liên kết', 'unyson' ),
		'before_widget' => '<div class="ht-widget-item ht-quick-links">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Khối đăng ký', 'unyson' ),
		'id'            => 'footer-widget-sub',
		'description'   => __( 'Các liên kết', 'unyson' ),
		'before_widget' => '<div class="ht-widget-item ht-subscribe">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	// Register sidebar for shop page,
	// remove it if theme dose not support Woocomerce
	if(class_exists('Woocommerce')) {
		register_sidebar(array(
			'name' => __('Thanh lề Shop', 'unyson'),
			'id' => 'sidebar-shop',
			'description' => __('', 'unyson'),
			'before_widget' => '<aside id="%1$s" class="widget shop %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		));
	}
}

add_action( 'widgets_init', '_action_theme_widgets_init' );


/**
 * Filter to wp_editor
 * to optimize fw_resize function
 */
add_filter( 'jpeg_quality', '_filter_theme_image_full_quality' );
add_filter( 'wp_editor_set_quality', '_filter_theme_image_full_quality' );

function _filter_theme_image_full_quality( $quality ) {
	return 100;
}

/*WOO*/
//Define theme supports Woo
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

//Remove default breadcrumb of Woo
add_action( 'init', 'fw_remove_wc_breadcrumbs' );
function fw_remove_wc_breadcrumbs() {
    remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
}

// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
    function loop_columns() {
        return 2; // 3 products per row
    }
}

//Remove default style of Woo
// add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

//Change number of products perpage
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 20;' ), 20 );

//Modify the price html, add a space between 2 prices
add_filter( 'woocommerce_get_price_html', 'wpa83367_price_html', 100, 2 );
function wpa83367_price_html( $price, $product ){
    return str_replace( '<ins>', '&nbsp;<ins>', $price );
}

/**
 * Change number of related products on product page
 */
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
  function jk_related_products_args( $args ) {
	$args['posts_per_page'] = 4; // 4 related products
	$args['columns'] = 2; // arranged in 2 columns
	return $args;
}

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
function woo_remove_product_tabs( $tabs ) {

    // unset( $tabs['description'] );      	// Remove the description tab
    // unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab
    return $tabs;
}

/**
* Make "state" field not required on checkout
*/
add_filter( 'woocommerce_billing_fields', 'woo_filter_state_billing', 10, 1 );
add_filter( 'woocommerce_shipping_fields', 'woo_filter_state_shipping', 10, 1 );
function woo_filter_state_billing( $address_fields ) {
	$address_fields['billing_city']['required'] = false;
	return $address_fields;
}
function woo_filter_state_shipping($address_fields){
	$address_fields['shipping_city']['required'] = false;
	return $address_fields;
}

add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs' );
function jk_woocommerce_breadcrumbs() {
    return array(
        'delimiter'   => ' <i class="fa fa-angle-right"></i> ',
        'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
    );
}