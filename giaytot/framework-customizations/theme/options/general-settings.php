<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'general' => array(
		'title'   => __( 'Chung', 'ht' ),
		'type'    => 'tab',
		'options' => array(
			'logo'    => array(
				'label' => __( 'Logo', 'ht' ),
				'type'  => 'upload',
			),
			'top_area' => array(
			    'type' => 'textarea',
			    'label' => esc_html__('Đỉnh trang', 'ht'),
			    'desc' => esc_html__('Nội dung của thanh đỉnh trang. Hãy dán mã HTML vào đây.', 'ht'),
			),
			'after_main_desc' => array(
			    'type' => 'wp-editor',
			    'label' => esc_html__('Giới thiệu', 'ht'),
			    'tinymce' => true,
			    'teeny' => false,
			    'size'  => 'large',
			    'editor_type' => 'tinymce',
			    'editor_height' => 500,
			    'reinit' => true,
			    'wpautop' => false,
			    'desc' => esc_html__('Mô tả sau khối nội dung chính', 'ht'),
			)
		)
	),
	'home' => array(
		'type' => 'tab',
		'title' => 'Trang nhà',
		'options' => array(
			'sale_off' => array(
				'type' => 'tab',
				'title' => 'Giảm giá',
				'options' => array(
					'sale_off_banner' => array(
						'type' => 'upload',
						'label' => 'Ảnh đại bên lề'
					),
					'sale_off_url' => array(
						'type' => 'text',
						'label' => 'URL'
					)
				)
			),
			'new_product' => array(
				'type' => 'tab',
				'title' => 'Sản phẩm mới',
				'options' => array(
					'new_product_banner' => array(
						'type' => 'upload',
						'label' => 'Ảnh đại bên lề'
					),
					'new_product_url' => array(
						'type' => 'text',
						'label' => 'URL'
					)
				)
			),
			'best_sale' => array(
				'type' => 'tab',
				'title' => 'Bán chạy nhất',
				'options' => array(
					'best_sale_banner' => array(
						'type' => 'upload',
						'label' => 'Ảnh đại bên lề'
					),
					'best_sale_url' => array(
						'type' => 'text',
						'label' => 'URL'
					)
				)
			),
		)
	),
	'banner_slider_settings' => array(
		'type' => 'tab',
		'title' => 'Banner slider',
		'options' => array(
			'site_banner' => array(
				'type' => 'addable-popup',
				'label' => 'Ảnh cho slider chính',
				'desc' => 'Các ảnh vuông có kích thước bằng nhau. Tốt nhất: 600x600',
				'template' => '{{=url}}',
				'popup-options' => array(
					'img' => array(
						'type' => 'upload'
					),
					'url' => array(
						'type' => 'text',
						'value' => home_url()
					)
				)
			),
			'special_block' => array(
				'type' => 'textarea',
				'label' => 'Khối đặc biệt',
				'desc' => 'Danh sách hiện dưới các danh mục',
				'help' => 'Xin chỉ thay URL và nhãn của các liên kết',
				'value' => '<div class="red-divider"></div>
							<ul class="product_cates">
								<li>
									<a href="http://localhost/giaytot-wp/mua/"><i class="fa fa-star-o"></i> Hàng mới về</a>
								</li>
								<li>
									<a href="http://localhost/giaytot-wp/tu-khoa/giam-gia/"><i class="fa fa-star-o"></i> Giảm giá</a>
								</li>
								<li>
									<a href="http://localhost/giaytot-wp/tu-khoa/ban-chay/"><i class="fa fa-star-o"></i> Bán chạy</a>
								</li>
							</ul>',
			),
			'right_banner_block' => array(
				'type' => 'textarea',
				'label' => 'khối bên phải',
				'desc' => 'Nội dung của cột bên tay phải của slides chính',
				'help' => 'Hãy thay đổi URL đích và URL của ảnh. Các ảnh nên có kích thước 280x300',
				'value' => '<a href="http://localhost/giaytot-wp/"><img src="http://localhost/giaytot-wp/wp-content/uploads/2016/04/4259.jpg" alt=""></a>
							<a href="http://localhost/giaytot-wp/"><img src="http://localhost/giaytot-wp/wp-content/uploads/2016/04/0311b8b.jpg" alt=""></a>'
			)
		)
	),
	'ad_popup_settings' => array(
		'type' => 'tab',
		'title' => 'Quảng Cáo',
		'options' => array(
			'ad_popup' => array(
				'type' => 'textarea',
				'label' => 'Nội dung Pop up',
				'desc' => 'Dán HTML ảnh vào đây'
			)
		)
	)
);