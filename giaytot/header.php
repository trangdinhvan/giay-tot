<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package _s
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php
wp_head();
global $woocommerce;
$qty = $woocommerce->cart->get_cart_contents_count();
$cart_url = $woocommerce->cart->get_cart_url();
$total = $woocommerce->cart->get_cart_total();
$checkout_url = $woocommerce->cart->get_checkout_url();
$top_area = '';
$logo = '';
if( function_exists('fw_get_db_settings_option') ){
	$top_area = fw_get_db_settings_option('top_area', '');
	$logo = fw_get_db_settings_option('logo', '');
}
?>
</head>

<body <?php body_class(); ?>>
<header id="site-header">
	<div class="top-area hidden-xs">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<?php echo html_entity_decode($top_area); ?>
				</div>
				 <div class="col-sm-6 sm-text-right user-links">
				 	 <?php if ( is_user_logged_in() ): ?>
				 	   <a class="my-account" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Tài khoản','woothemes'); ?>"><?php _e('Tài khoản','woothemes'); ?></a>
				 	   <a href="<?php echo $cart_url; ?>"><?php _e("Giỏ hàng","stplus"); ?></a>
				 	   <a href="<?php echo $checkout_url; ?>"><?php _e("Thanh toán","stplus"); ?></a>
				 	    <a class="logout" href="<?php echo wp_logout_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ); ?>">Đăng xuất</a>
				 	<?php else: ?>
				 	    <a class="logout" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Đăng nhập','woothemes'); ?>"><?php _e('Đăng nhập','woothemes'); ?></a>
				 	    <a class="logout" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Đăng ký','woothemes'); ?>"><?php _e('Đăng ký','woothemes'); ?></a>
				 	<?php endif; ?>
				 </div>
			</div>
		</div>
	</div>
	<div class="top-group">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div id="site-logo">
						<?php
							echo $logo != '' ? '<a href="'.home_url().'"><img src="'.$logo['url'].'" alt="'.get_bloginfo('name').'"></a>' : '';
						?>
					</div>
				</div>
				<!-- /#site-logo -->
				<div class="col-lg-7 col-sm-6">
					<div class="shop-search">
						<form action="">
							<?php
								$search_value = isset($_GET['s']) && isset($_GET['post_type']) && $_GET['post_type'] == 'product' ? $_GET['s'] : '';
							?>
						    <input type="text" name="s" class="w3-input w3-border" placeholder="<?php _e('Nhập từ khóa tìm kiếm', 'fw'); ?>" value="<?php echo $search_value; ?>">
						    <input type="hidden" name="post_type" value="product">
						    <p style="margin-top: 0;">Gợi ý từ khóa:
						    	<?php
						    		$number = 5;
						    		if( wp_is_mobile() ){
						    			$number  = 3;
						    		}
			                        $terms = get_terms(
			                        	"product_tag",
			                        	array(
			                        		'number' => $number,
			                        		'orderby' => 'count',
			                        		'order' => 'DESC'
			                        	)
			                        );
			                        if( !is_wp_error($terms) && !empty($terms) ):
			                            foreach ( (array)$terms as $k => $v) {
			                                echo '&nbsp;<strong><a href="'.get_term_link( $v->slug, 'product_tag' ).'">'.$v->name.'</a></strong>,';
			                            }
			                        endif;
			                        echo '...'
			                    ?>
						    </p>
						</form>
					</div>
				</div>
				<!-- /.shop-search -->
				<div class="col-lg-2 col-sm-3">
					<div class="cart-indicator">
						<?php
						    echo '<a href="'.$cart_url.'"><i class="fa fa-shopping-cart"></i>'.$qty.' <small>sản phẩm</small>&nbsp;&nbsp;</a>';
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<nav id="site-navigation">
		    <?php
		        $menu_position = 'primary';
		        if ( has_nav_menu( $menu_position ) ) {
		            wp_nav_menu(array(
		                'theme_location' => $menu_position,
		                'menu_class'      => 'menu '.$menu_position,
		                'container_id' => 'cssmenu'
		            ));
		        }
		    ?>
		</nav>
	</div>
</header>
<div class="container">
	<div id="site-breadcrumb">
		<?php
		    function_exists('woocommerce_breadcrumb') ? woocommerce_breadcrumb() : '';
		?>
	</div>
</div>
