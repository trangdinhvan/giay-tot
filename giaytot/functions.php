<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

/**
 * Theme Includes
 */
require_once get_template_directory() .'/inc/init.php';

/**
 * TGM Plugin Activation
 */
{
	require_once dirname( __FILE__ ) . '/TGM-Plugin-Activation/class-tgm-plugin-activation.php';

	require_once dirname( __FILE__ ) . '/TGM-Plugin-Activation/recommend_plugins.php';
}

function conditionally_load_woc_js_css(){
if( function_exists( 'is_woocommerce' ) ){
        # Only load CSS and JS on Woocommerce pages
    if( !is_cart() && !is_checkout() ) {

        ## Dequeue scripts.
        // wp_dequeue_script('woocommerce');
        // wp_dequeue_script('wc-add-to-cart');
        // wp_dequeue_script('wc-cart-fragments');

        ## Dequeue styles.
        wp_dequeue_style('woocommerce-general');
        wp_dequeue_style('woocommerce-layout');
        wp_dequeue_style('woocommerce-smallscreen');

        }
    }
}

add_action( 'wp_enqueue_scripts', 'conditionally_load_woc_js_css' );