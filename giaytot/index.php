<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>
<main id="main" class="container">
	<?php dynamic_sidebar('ad-banner-top'); ?>
	<div class="row">
		<div id="primary" class="col-sm-12">
			<section id="home-banner-group" class="ht-section">
				<div class="container-fluid">
					<div class="row no-gutter">
						<div class="col-sm-3 hidden-xs col-left">
							<?php
								 $pro_cate_args = array(
								        'taxonomy'     => 'product_cat',
								        'orderby'      => 'count',
								        'order' => 'DESC',
								        'show_count'   => 0,
								        'pad_counts'   => 0,
								        'hierarchical' => 0,
								        'title_li'     => false,
								        'hide_empty'   => 0
								 );
								$pro_cates = get_categories( $pro_cate_args );
								if( !is_wp_error($pro_cates) && !empty($pro_cates) ):
									echo '<ul class="product_cates">';
									foreach ($pro_cates as $cat) {
										?>
											<li>
												<a href="<?php echo get_term_link( $cat->slug, 'product_cat' ); ?>"><i class="fa fa-bookmark-o"></i>&nbsp; <?php echo $cat->name; ?></a>
												<?php
													$cat_img = function_exists('fw_get_db_term_option') ? fw_get_db_term_option($cat->term_id, 'product_cat', 'banner_quare', '' ): '';
													if( $cat_img != '' ){
														?>
															<a href="<?php echo get_term_link( $cat->slug, 'product_cat' ); ?>"><img src="<?php echo $cat_img['url']; ?>" alt=""></a>
														<?php
													}
												?>
												<i class="fa fa-hand-o-right"></i>
											</li>
										<?php
									}
									echo '</ul>';
								endif;
							?>
							</ul>
							<?php
								$special_block = function_exists('fw_get_db_settings_option') ? fw_get_db_settings_option('special_block', '') : '';
								if( $special_block != '' ){
									echo html_entity_decode($special_block);
								}
							?>
						</div>
						<div class="col-sm-6">
							<?php
								$home_slides = function_exists('fw_get_db_settings_option') ? fw_get_db_settings_option('site_banner', array()) : array();
								if( !empty($home_slides) ):
									echo '<div class="ht-flexslider" id="site-slider" data-pager="true" data-navi="false">';
									echo '<ul class="slides">';
									foreach ($home_slides as $key => $v) {
										if( isset($v['img']['url']) ){
											echo '<li>';
											echo '<a href="'.$v['url'].'"><img src="'.$v['img']['url'].'" alt=""></a>';
											echo '</li>';
										}
									}
									echo '</ul>';
									echo '</div>';
								endif;
							?>
						</div>
						<div class="col-sm-3 hidden-xs">
							<?php
								$after_banner = function_exists('fw_get_db_settings_option') ? fw_get_db_settings_option('right_banner_block', '') : '';
								echo html_entity_decode($after_banner);
							?>
						</div>
					</div>
				</div>
			</section>
			<!-- /#home-banner-group -->
			<br>
			<section id="sale-off" class="ht-section products-list">
				<div class="row">
					<div class="col-md-3 col-sm-4 hidden-xs">
						<br>
						<?php
							$sale_off_banner = function_exists('fw_get_db_settings_option') ? fw_get_db_settings_option('sale_off_banner', array()) : array();
							$sale_off_url = function_exists('fw_get_db_settings_option') ? fw_get_db_settings_option('sale_off_url', '') : '';
							if( !empty($sale_off_banner) ){
								echo '<a href="'.$sale_off_url.'"><img src="'.$sale_off_banner['url'].'" alt=""></a>';
							}
						?>
					</div>
					<div class="col-md-9 col-sm-8">
						<div class="row">
							<div class="col-xs-6">
								<div class="list-title text-uppercase">Đang giảm giá</div>
							</div>
							<div class="col-xs-6 text-right">
								<a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>" class="list-link">Xem tất cả <i class="fa fa-angle-right"></i></a>
							</div>
						</div>
						<?php
							echo do_shortcode('[sale_products per_page="8" columns="2"]');
						?>
					</div>
				</div>
			</section>
			<!-- /#sale-off -->

			<section id="new-products" class="ht-section products-list">
				<div class="row">
					<div class="col-md-9 col-sm-8">
						<div class="row">
							<div class="col-xs-6">
								<div class="list-title text-uppercase">Hàng mới về</div>
							</div>
							<div class="col-xs-6 text-right">
								<a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>" class="list-link">Xem tất cả <i class="fa fa-angle-right"></i></a>
							</div>
						</div>
						<?php
							echo do_shortcode('[recent_products per_page="8" columns="2"]');
						?>
					</div>
					<div class="col-md-3 col-sm-4 hidden-xs">
						<br>
						<?php
							$new_product_banner = function_exists('fw_get_db_settings_option') ? fw_get_db_settings_option('new_product_banner', array()) : array();
							$new_product_url = function_exists('fw_get_db_settings_option') ? fw_get_db_settings_option('new_product_url', '') : '';
							if( !empty($new_product_banner) ){
								echo '<a href="'.$new_product_url.'"><img src="'.$new_product_banner['url'].'" alt=""></a>';
							}
						?>
					</div>
				</div>
			</section>
			<!-- /#new-products -->

			<section id="best-sale" class="ht-section products-list">
				<div class="row">
					<div class="col-md-3 col-sm-4 hidden-xs">
						<br>
						<?php
							$best_sale_banner = function_exists('fw_get_db_settings_option') ? fw_get_db_settings_option('best_sale_banner', array()) : array();
							$best_sale_url = function_exists('fw_get_db_settings_option') ? fw_get_db_settings_option('best_sale_url', '') : '';
							if( !empty($best_sale_banner) ){
								echo '<a href="'.$best_sale_url.'"><img src="'.$best_sale_banner['url'].'" alt=""></a>';
							}
						?>
					</div>
					<div class="col-md-9 col-sm-8">
						<div class="row">
							<div class="col-xs-6">
								<div class="list-title text-uppercase">Sản phẩm bán chạy nhất</div>
							</div>
							<div class="col-xs-6 text-right">
								<a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>" class="list-link">Xem tất cả <i class="fa fa-angle-right"></i></a>
							</div>
						</div>
						<?php
							echo do_shortcode('[best_selling_products per_page="8" columns="2"]');
						?>
					</div>
				</div>
			</section>
			<!-- /#best-sale -->
		</div>
	</div>
</main>
<?php get_footer(); ?>
