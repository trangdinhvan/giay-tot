;(function($) {
    "use strict";
    var HAINTHEME = HAINTHEME || {};
    var previousScroll = 0;

    function resizeend() {
        if (new Date() - rtime < delta) {
            setTimeout(resizeend, delta);
        } else {
            timeout = false;
            var w = $('#site-slider ul.slides > li').width();

            $('#home-banner-group li img').css('max-width', w + 'px');
            $('#home-banner-group .col-left').css('min-height', w + 'px');
        }
    }

    jQuery(document).ready(function($) {
        $('.ht-flexslider').flexslider({
            animation: "slide",
            controlNav      :   $(this).data('pager'), // 1, 2, 3,
            directionNav    :   false
        });
        var w = $('#site-slider ul.slides > li').width()/10;
        $('#home-banner-group li img').css('max-width', w + 'px');
        $('#home-banner-group .col-left').css('min-height', w + 'px');
    });

    var rtime;
    var timeout = false;
    var delta = 200;

    $(window)
    .on( 'load', function() {

    })
    .on( 'resize', function() {
        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(resizeend, delta);
        }
    })
    .on( 'scroll', function() {
    });
})(jQuery);